<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="GuardRange" version="1.0" date="10/06/2020" >

		<Author name="Sullemunk"/>
		<Description text="MultiGuard Range" />
		
        <Dependencies>
			<Dependency name="LibGuard" />
        </Dependencies>
        
        <SavedVariables>
        </SavedVariables>
        
		<Files>
			<File name="GuardRange.xml" />
		</Files>
		
		<OnInitialize>
            <CallFunction name="GuardRange.Initialize" />
		</OnInitialize>
		<OnUpdate>
        </OnUpdate>
		<OnShutdown/>
		
	</UiMod>
</ModuleFile>
